import numpy as np
import itertools
import random
from bisect import bisect_right
from scipy.optimize import minimize
import scipy.stats as stats
from scipy.special import zeta
import functools

def gen_samp_scipy_zipf(alpha = 1.8, size = int(1e6)) :
    samp = stats.zipf.rvs(a = alpha, size = size)
    counts = {}
    for s in samp :
        if s not in counts :
            counts[s] = 1
        else :
            counts[s] += 1
    freqs = [x[1] for x in counts.items()]
    freqs.sort(reverse = True)
    return freqs

def gen_samp_log_normal_interval(mu = 1, sigma = 1, size = int(1e6), rMax = 10000) :
    const = get_const_lognorm(mu, sigma)
    widths = [eval_lognormal(r, mu, sigma) for r in range(1, rMax + 1)]
    bounds = [sum(widths[:x]) for x in range(1, rMax)] + [1]

    freqs = [0 for x in range(rMax)]
    for i in range(size) :
        r = random.random()
        ind = bisect_right(bounds, r)
        freqs[ind] += 1
    return sorted(freqs, reverse = True)



def gen_samp_interval(alpha = 1.8, size = 1e6, maxRank = 10000, normalized = False) :
    rMax = maxRank
    const = 1 / sum((r**-alpha for r in range(1, rMax + 1)))
    widths = [const * r**-alpha for r in range(1, rMax + 1)]
    bounds = [sum(widths[:x]) for x in range(1, rMax)] + [1]

    freqs = [0 for x in range(1, rMax + 1)]
    for i in range(int(size)) :
        r = random.random()
        ind = bisect_right(bounds, r)
        freqs[ind] += 1

    if not normalized :
        return sorted(freqs, reverse = True)
    else :
        total = sum(freqs)
        return sorted((f/total for f in freqs), reverse = True)

def gen_samp_dpl_interval(alp1, alp2, b, size = int(1e6), rMax = 10000) :
    const = get_const_dpl(alp1, alp2, b)
    constB = const * b**(alp2-alp1)
    widths = [const * r**-alp1 for r in range(1, b)] + [constB * r**-alp2 for r in range(b, rMax+1)]
    bounds = [sum(widths[:x]) for x in range(1, rMax)] + [1]
    freqs = [0 for x in range(1, rMax + 1)]
    for i in range(int(size)) :
        r = random.random()
        ind = bisect_right(bounds, r)
        freqs[ind] += 1
    freqs.sort(reverse = True)
    return freqs

def gen_func(alpha, xMin, plt = None) :
    const = get_const(alpha, xMin)
    xAx = np.logspace(np.log10(xMin), 4, 50)
    yAx = [const * r**-alpha for r in xAx]
    try :
        return plt.loglog(xAx, yAx)
    except : return xAx, yAx

def gen_func_dpl(alp1, alp2, b, xMin = 1, mplInstance = None) :
    xAx = np.logspace(np.log10(xMin), 4, 50)
    const = get_const_dpl(alp1, alp2, b, xMin)
    constB = const * b**(alp2-alp1)
    yAx = [const * x**-alp1 for x in xAx if x < b]
    yAx += [constB * x**-alp2 for x in xAx if x >= b]
    try :
        mplInstance.plot(xAx, yAx)
    except : pass
    return xAx, yAx

def gen_func_lognorm(mu, sigma, plt = None, xMin = 1) :
    xAx = np.logspace(np.log10(xMin), 4, 50)
    yAx = [eval_lognormal(r, mu, sigma) for r in xAx]
    try :
        plt.loglog(xAx, yAx)
    except : return xAx, yAx

def plot_freqs(plt, freqs, xMin = 1, normalized = True, plotAll = True, kwargs = {}) :
    tail = freqs[xMin-1:]
    if normalized :
        total = sum(tail)
        freqs = [f/total for f in freqs]

    if plotAll :
        plt.loglog(list(range(len(freqs))), freqs, **kwargs)
    else :
        tail = freqs[xMin-1:]
        plt.loglog(list(range(xMin, len(tail) + xMin)), tail, **kwargs)

def eval_lognormal(rank, mu, sigma, const = None) :
    const = get_const_lognorm(mu, sigma) if const is None else const
    return const * rank**-1 * np.exp(-0.5 * (np.log(rank) - mu)**2 / sigma**2)

def gen_cdf_func(alpha, xMin = 1, accuracy = 4, plt = None) :
    const = get_const(alpha, xMin)
    cdf = list(itertools.accumulate((const * r**-alpha for r in range(xMin, int(10**accuracy)))))
    if plt :
        plt.loglog(range(xMin, len(cdf) + xMin), cdf)
    return cdf

def get_cdf(freqs, xMin = 1, plt = None) :
    total = sum(itertools.islice(freqs, xMin-1, None))
    cdf =  list(itertools.accumulate((f/total for f in itertools.islice(freqs, xMin-1, None))))
    if plt :
        plt.loglog(range(xMin, len(cdf) + xMin), cdf)
    return cdf

def get_KS_stat(freqs, xMin = 1) :
    aHat = alphaHat_max_likelihood(freqs, xMin)
    const = get_const(aHat, xMin)
    total = sum(itertools.islice(freqs, xMin-1, None))
    stat = min((abs(f/total - const * r**-aHat) for r, f, in enumerate(itertools.islice(freqs, xMin-1, None), xMin)))
    return stat

def get_const(alp, xMin = 1, accuracy = 4) :
    return 1/sum(x**-alp for x in range(xMin, 10**accuracy))

@functools.lru_cache()
def get_const_lognorm(mu, sigma, xMin = 1, accuracy = 4) :
    return 1/sum(r**-1 * np.exp(-0.5 * (np.log(r) - mu)**2 /sigma**2) for r in range(xMin, int(10**accuracy)))

@functools.lru_cache()
def get_const_dpl(alp1, alp2, b, xMin = 1, accuracy = 4) :
    b = int(b)
    result = sum(x**-alp1 for x in range(xMin, b))
    result += sum(b**(alp2-alp1) * x**-alp2 for x in range(b, 10**accuracy))
    return 1/result

def get_freq_for_rank_dpl(rank, a1, a2, b) :
    const = get_const_dpl(a1, a2, b)
    constB = const * b**(a2 - a1)
    if rank <= b :
        return const * rank**-a1
    else :
        return constB * rank**-a2

def get_likelihood_dpl_fr(data, a1, a2, b) :
    total = sum(data)
    data = [x/total for x in data]
    ll = -sum(num * np.log(get_freq_for_rank_dpl(r, a1, a2, b)) for r, num in enumerate(data, 1))
    return ll


def get_likelihood_lognorm(x, args) :
    mu, sigma = x
    freqs, xMin = args
    const = get_const_lognorm(mu, sigma, xMin)
    return -sum(num * np.log(const * r**-1 * np.exp(-0.5 * (np.log(r) - mu)**2 /sigma**2)) for r, num in enumerate(freqs[xMin-1:], xMin))

def get_likelihood_lognorm_normalized(mu, sigma, freqs, xMin = 1) :
    tail = freqs[xMin-1:]
    total = sum(tail)
    freqs = [f/total for f in freqs]
    return get_likelihood_lognorm((mu, sigma), (freqs, xMin))

def get_likelihood_dpl_one_param(x, args) :
    freqs, xMin, x0, pInd = args

    x = x[0]
    x0[pInd] = x

    return get_likelihood_dpl(x0, [freqs, xMin])

def get_likelihood_dpl_normalized(params, freqs, xMin = 1) :
    tail = freqs[xMin-1:]
    total = sum(tail)
    freqs = [f/total for f in freqs]
    return get_likelihood_dpl(params, [freqs, xMin])

def get_likelihood_dpl(x, args) :

    alp1, alp2, b = x
    freqs, xMin = args
    b = int(b)
    const = get_const_dpl(*x, xMin)
    constB = const * b**(alp2 - alp1)
    result = sum(num * np.log(const * r**-alp1) for r, num in enumerate(freqs[xMin-1:b], xMin))
    result += sum(num * np.log(constB * r**-alp2) for r, num in enumerate(freqs[b:], b+xMin))
    return -result

def fit_dpl_max_likelihood_one_param(freqs, x0, bounds = [(0.5, 3), (1, 3), (10, 1000)], method = None, iniConds = [1.0, 1.5, 200]) :
    pInd = [i for i, p in enumerate(x0) if p is None][0] #index of parameter to vary
    x0[pInd] = iniConds[pInd]
    if method :
        result = minimize(
            get_likelihood_dpl_one_param,
            x0[pInd],
            args = [freqs, 1, x0, pInd],
            method = method,
        )
        x0[pInd] = result['x'][0]
        return x0, get_likelihood_dpl(x0, [freqs, 1])
    else :
        result = minimize(
            get_likelihood_dpl_one_param,
            x0[pInd],
            args = [freqs, 1, x0, pInd],
            bounds = [bounds[pInd]],
        )
        x0[pInd] = result['x'][0]
        return x0, get_likelihood_dpl(x0, [freqs, 1])


def fit_dpl_max_likelihood(freqs, xMin = 1, x0 = [1.0, 1.5, 200], bounds = [(0.5, 3), (1, 3), (10, 1000)], method = None) :
    #freqs = freqs[xMin-1:]
    if method :
        result = minimize(
            get_likelihood_dpl,
            x0,
            args = [freqs, xMin],
            method = method,
        )
        return result['x'], get_likelihood_dpl_normalized(result['x'], freqs, xMin)
    else :
        result = minimize(
            get_likelihood_dpl,
            x0,
            args = [freqs, xMin],
            bounds = bounds,
        )
        return result['x'], get_likelihood_dpl_normalized(result['x'], freqs, xMin)

def fit_lognorm_max_likelihood(freqs, xMin = 1, x0 = [1, 10], bounds = [(0.01, 100), (0.01, 100)], method = None) :
    if method :
        result = minimize(
            get_likelihood_lognorm,
            x0,
            args = [freqs, xMin],
            bounds = bounds,
        )
        return result['x'], get_likelihood_lognorm_normalized(*result['x'], freqs, xMin)
    else :
        result = minimize(
            get_likelihood_lognorm,
            x0,
            args = [freqs, xMin],
            method = method,
        )
        return result['x'], get_likelihood_lognorm_normalized(*result['x'], freqs, xMin)

def get_likelihood_normalized(alpha, freqs, xMin = 1) :
    freqs = freqs[xMin-1:]
    total = sum(freqs)
    freqs = [f/total for f in freqs]
    return get_likelihood(alpha, (freqs, xMin))

def get_likelihood(alp, args) :
    freqs, xMin = args
    const = get_const(alp, xMin)
    return -sum(num * np.log(const * r**-alp) for r, num in enumerate(freqs, xMin) if num > 0)
#xMin is minimum rank (starts at 1 not 0)
def fit_max_likelihood(data, xMin = 1) :
    data = data[xMin-1:]
    total = sum(data)
    data = [d/total for d in data]
    result = minimize(get_likelihood, 1.5, args = [data, xMin], bounds = [(1, 3)])
    return result['x'][0]
